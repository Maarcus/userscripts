// ==UserScript==
// @name        Fanhouse - Download Buttons
// @namespace   github.com/M-rcus
// @match       https://fanhouse.app/*
// @downloadUrl https://gist.github.com/M-rcus/a20d4f1a3d09370fb99590f346dfbb69/raw/fanhouse.user.js
// @updateUrl   https://gist.github.com/M-rcus/a20d4f1a3d09370fb99590f346dfbb69/raw/fanhouse.user.js
// @grant       GM_download
// @grant       GM_addStyle
// @version     1.3.5
// @author      Marcus
// @description Remove watermark overlays + add download buttons for images & videos.
// ==/UserScript==

/**
 * ================ ** WARNING ** ================
 * There's a chance that your account gets suspended. I'm not sure what the criteria is for suspension,
 * but maybe don't go crazy downloading 40 posts in 3 seconds - though I have no idea if that helps at all.
 *
 * Sorry for anyone that has already gotten their account suspended. That was never my intention, though in hindsight
 * probably would've been good to add warnings about.
 *
 * Cheers.
 */

const watermarkIdMatch = 'div[id*="watermark-body"], div[id*="water-mark-body"], div[style*="z-index: 9999;"]';

/**
 * Script isn't the greatest, probably has a few bugs and design-wise it's ugly as fuck.
 */

function toggleIcons(icon)
{
    if (!icon) {
        return;
    }

    if (icon.classList.contains('fa-download')) {
        icon.classList.add('fa-spin');
        icon.classList.add('fa-spinner');
        icon.classList.remove('fa-download');
        return;
    }

    if (icon.classList.contains('fa-spin')) {
        icon.classList.remove('fa-spin');
        icon.classList.remove('fa-spinner');
        icon.classList.add('fa-download');
    }
}

let alreadyChecked = [];

function downloadUrl(url, filename, elem)
{
    if (elem) {
        if (elem.getAttribute('disabled') === '1') {
            return;
        }

        elem.setAttribute('disabled', '1');
        toggleIcons(elem);
    }

    const downloadParams = {
        method: 'GET',
        url: url,
        name: filename,
        saveAs: true,
        headers: {
            Referer: window.location.href,
            'User-Agent': navigator.userAgent,
        },
        onload: (response) => {
            if (elem) {
                setTimeout(
                    function()
                    {
                        elem.removeAttribute('disabled');
                        toggleIcons(elem);
                    },
                    250
                );
            }
        },
        // TODO: Progress indicator might be handy, at least for videos.
        onprogress: (results) => {},
        onerror: (obj) => {
            console.error(obj);
        },
    };

    GM_download(downloadParams);
}

function handleVideo(video, buttons)
{
    const videoSrc = video.getAttribute('src');
    if (alreadyChecked.includes(videoSrc)) {
        return;
    }

    alreadyChecked.push(videoSrc);

    if (document.querySelector(`a[href="${videoSrc}"]`)) {
        return;
    }

    const videoUrl = new URL(videoSrc);
    const videoPath = videoUrl.pathname.split('/');
    const lastPath = videoPath.pop();

    if (!lastPath || lastPath.length < 3) {
        return;
    }

    let filename = decodeURIComponent(lastPath).split('/').pop();
    if (!filename) {
        return;
    }

    const timestampSection = filename.substring(0, 26);
    const fileTimestamp = Date.parse(timestampSection);
    if (!isNaN(fileTimestamp) && fileTimestamp > 86400) {
        const date = (new Date(fileTimestamp)).toISOString().split('T')[0];
        console.log(date);
        filename = `${date}_${filename}`;
    }

    const videoBtn = document.createElement('div');
    videoBtn.setAttribute('class', 'col-6 center');
    videoBtn.innerHTML = `
            <button class="btn-nofrills">
                <i class="fa fa-download fa-2x fa-outline"></i>
            </button>

            <p>Download video</p>
    `;

    videoBtn.addEventListener('click', function(ev) {
        downloadUrl(videoSrc, filename, ev.target);
    });

    buttons.insertAdjacentElement('beforeend', videoBtn);
}

function handleImage(image, wrapper)
{
    const imageSrc = image.src;

    if (alreadyChecked.includes(imageSrc)) {
        return;
    }

    alreadyChecked.push(imageSrc);

    const imageUrl = new URL(imageSrc);
    const imagePath = imageUrl.pathname.split('/');
    const lastPath = imagePath.pop();

    if (!lastPath || lastPath.length < 3) {
        return;
    }

    let filename = decodeURIComponent(lastPath).split('/').pop();
    if (!filename) {
        return;
    }

    const timestampSection = filename.substring(0, 26);
    const fileTimestamp = Date.parse(timestampSection);
    if (!isNaN(fileTimestamp) && fileTimestamp > 86400) {
        const date = (new Date(fileTimestamp)).toISOString().split('T')[0];
        console.log(date);
        filename = `${date}_${filename}`;
    }

    const btn = document.createElement('button');
    btn.innerHTML = '<i class="fa fa-download fa-1x"></i>';

    btn.addEventListener('click', function(ev) {
        downloadUrl(imageSrc, filename, ev.target);
    });

    wrapper.querySelector('div').insertAdjacentElement('beforebegin', btn);
}

function fuckOldWatermarks()
{
    const watermarks = document.querySelectorAll(watermarkIdMatch);
    for (const watermark of watermarks)
    {
        watermark.remove();
    }
}

function handleMutationsList(list)
{
    const posts = document.querySelectorAll('.section-post-component, .section-post');
    const mediaFrame = '.media-frame--post, .media-frame-post';

    for (const post of posts)
    {
        /**
         * Completely watermark element from DOM.
         *
         * I don't think removing the elements from DOM are strictly necessary,
         * now that they're hidden via CSS, but it doesn't seem to hurt to keep this.
         */
        const watermark = post.querySelector(watermarkIdMatch);
        if (watermark) {
            watermark.remove();
        }

        const media = post.querySelector(mediaFrame);
        if (!media) {
            continue;
        }

        const buttons = post.querySelector('.section-post-like-reply > .row, .section-post-likereply > .row');

        /**
         * Video
         */
        const video = media.querySelector('video');
        if (video) {
            handleVideo(video, buttons);
            // Continue because video posts cannot contain images, gg Fanhouse
            continue;
        }

        /**
         * Images
         */
        const slides = media.querySelectorAll('.slide');
        for (const slide of slides)
        {
            const wrapper = slide.querySelector(mediaFrame);
            const image = slide.querySelector('img');
            handleImage(image, wrapper);
        }
    }

    const messages = document.querySelectorAll('.chat-bubble');
    for (const message of messages)
    {
        /**
         * Completely watermark element from DOM.
         */
        const watermark = message.querySelector(watermarkIdMatch);
        if (watermark) {
            watermark.remove();
        }

        const media = message.querySelector('.img-chat-bubble');
        if (!media) {
            continue;
        }

        /**
         * TODO: Video support
         */

        /**
         * Images
         */
        const frames = message.querySelectorAll('div[data-rmiz-wrap*="e"], .media-frame');
        for (const frame of frames)
        {
            const image = frame.querySelector('img');
            const wrapper = frame.classList.contains('media-frame') ? frame : frame.parentElement;
            handleImage(image, wrapper);
        }
    }
}

window.addEventListener('DOMContentLoaded', function() {
    const observer = new MutationObserver(handleMutationsList);
    observer.observe(document.querySelector("#root"), {
        subtree: true,
        childList: true,
    });
});

/* Hide watermarks using CSS */
GM_addStyle(`${watermarkIdMatch} { display: none !important; }`);
