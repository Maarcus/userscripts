// ==UserScript==
// @name        Sproutvideos (*.vids.io) - Get video embed URL.
// @namespace   github.com/M-rcus
// @match       https://*.vids.io/videos/*
// @grant       none
// @version     1.2.0
// @author      Marcus
// @description Gets the video embed URL for Sproutvideo.
// ==/UserScript==

window.addEventListener('load', () => {
    const player = document.querySelector('.sproutvideo-player');

    if (!player)
    {
        return;
    }

    const url = player.src;
    const container = document.querySelector('body');
    const html = `<div class="video-content group"><p class="duration"><a href="${url}" target="_blank">${url}</p></div>`;
    container.insertAdjacentHTML('afterbegin', html);
});