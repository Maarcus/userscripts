const articles = Array.from(document.querySelectorAll('article'));
const commands = [];

for (const art of articles)
{
    const titleElem = art.querySelector('.title > a');
    const title = titleElem.textContent.trim();

    const date = (art.querySelector('time.time-ago').textContent).trim();

    const name = `'${title} - ${date}.mp4'`;

    const source = art.querySelector('video > source');
    commands.push(`curl -L -o ${name} ${source.src}`);
}

`\n${commands.join('\n')}\n`