// ==UserScript==
// @name        OnlyLeaksSucks
// @namespace   github.com/M-rcus
// @match       https://www.onlyleaks.me/*
// @match       https://onlyleaks.me/*
// @grant       none
// @version     1.1.0
// @author      Maarcus
// @description Userscripts that gets rid of ad shit on the page... hopefully.
// ==/UserScript==

window.addEventListener('load', () => {
    /**
     * Removes some of the ad stuff, I still recommend an adblocker like Ublock Origin
     */
    const scripts = Array.from(document.querySelectorAll('script[data-cfasync="false"]'));
    for (const script of scripts)
    {
        script.remove();
    }
    
    /**
     * Make URLs proper 'anchor' tags so you can open them in a new tab.
     */
    const urls = Array.from(document.querySelectorAll('td[data-url]:not([data-url=""])'));
    for (const urlElement of urls)
    {
        const url = urlElement.getAttribute('data-url');
        const textHtml = urlElement.querySelector('.x3');
        const text = textHtml.innerHTML;
        
        /**
         * The 'onclick' event should no longer be necessary as we now have a clickable link.
         */
        urlElement.onclick = null;
        textHtml.innerHTML = `<a href="${url}" style="color: #fff;">${text}</a>`;
    }
});