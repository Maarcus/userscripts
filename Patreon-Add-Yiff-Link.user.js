// ==UserScript==
// @name        Patreon - Add Yiff.Party Link
// @namespace   github.com/M-rcus
// @match       https://www.patreon.com/*
// @match       https://www.patreon.com/m/*
// @grant       none
// @version     1.0.0
// @author      Maarcus
// @grant       unsafeWindow
// @description Adds a link to the Yiff.party page somewhere on the page.
// ==/UserScript==

const log = (input) => {
    if (typeof input === 'string' || typeof input === 'number') {
        input = `[Patreon - Add Yiff.Party Link] ${input}`;
    }

    console.log(input);
};

const logError = (input) => {
    if (typeof input === 'string' || typeof input === 'number') {
        input = `[Patreon - Add Yiff.Party Link] ${input}`;
    }

    console.error(input);
};

const addYiffLink = async (id) => {
    const url = `https://yiff.party/patreon/${id}`;
    const navBar = document.querySelector('div[data-tag="navbar"]');
    const header = navBar.querySelector('header');
    const homepageLink = navBar.querySelector('a[aria-label="Go to home page"]');
    const classes = homepageLink.className;

    const urlBtn = `<a href="${url}" target="_blank" id="yiff-link" class="${classes}" rel="noopener noreferrer"><img style="width: 24px; height: 24px;" src="https://yiff.party/static/img/icon.png"></a>`;
    homepageLink.insertAdjacentHTML('afterend', urlBtn);
};

window.addEventListener('DOMContentLoaded', async () => {
    // Can't find a good way to reliable figure out when the DOM is ready
    // so this is what I came up with...
    setTimeout(async () => {
        try {
            /**
             * This is some real ghetto shit, but idc.
             */
            const bootstrap = unsafeWindow.patreon.bootstrap;
            const parentData = bootstrap.campaign || bootstrap.creator;
            const id = parentData
                      .data
                      .relationships
                      .creator
                      .data
                      .id;

            log(`Found creator ID: ${id} - Attempting to add Yiff link`);

            await addYiffLink(id);
        } catch (err) {
            logError(
                'Could not find Patreon creator ID, not adding Yiff.party link.',
            );
            logError(err);
        }
    }, 1500);
});
