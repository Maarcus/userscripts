// ==UserScript==
// @name        SuicideGirls - Copy album URLs
// @namespace   github.com/M-rcus
// @match       https://www.suicidegirls.com/girls/*/photos/
// @match       https://www.suicidegirls.com/members/*/photos/
// @grant       none
// @version     1.1.0
// @author      Maarcus
// @description 2/2/2020, 2:26:32 PM
// ==/UserScript==

function copyToClipboard(str)
{
    const el = document.createElement('textarea');
    el.value = str;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}

let urls = [];
async function getUrls()
{
    const elements = Array.from(document.querySelectorAll('.title > a'));
    urls = elements.map(a => a.href);
}

async function addCopyBtn()
{
    const button = document.createElement('button');
    button.appendChild(document.createTextNode(`Copy ${urls.length} album URLs to clipboard`));

    button.addEventListener('click', function() {
        copyToClipboard(urls.join('\n'));
        alert(`Copied ${urls.length} album URLs to clipboard.`);
    }, false);

    const content = document.querySelector('#content-column');
    content.insertAdjacentElement('afterbegin', button);
}

async function clickLoadMore()
{
    const loadMore = document.querySelector('a#load-more');

    if (!loadMore) {
        return false;
    }

    if (loadMore.parentElement.style.display === "none") {
        return false;
    }

    loadMore.click();
    return true;
}

window.addEventListener('load', async () => {
    const interval = setInterval(async () => {
        const shouldLoadMore = await clickLoadMore();

        if (!shouldLoadMore) {
            clearInterval(interval);
            await getUrls();
            await addCopyBtn();
        }
    }, 1000);
});