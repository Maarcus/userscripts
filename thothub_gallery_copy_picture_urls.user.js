// ==UserScript==
// @name        Thothub.tv - Copy gallery picture URLs to Clipboard
// @namespace   https://github.com/M-rcus
// @match       https://thothub.tv/*
// @grant       none
// @version     1.1.0
// @author      Marcus
// @description Copies the direct gallery URLs to your clipboard, so you can use it with software like JDownloader or similar.
// @updateUrl https://gist.github.com/M-rcus/2bb243980a1a9979d328606a8038d270/raw/thothub_gallery_copy_picture_urls.user.js
// ==/UserScript==

/**
 * Shamelessly copied from: https://techoverflow.net/2018/03/30/copying-strings-to-the-clipboard-using-pure-javascript/
 */
function copyStringToClipboard (str) {
    if (Array.isArray(str))
    {
        str = str.join('\r\n');
    }

    // Create new element
    var el = document.createElement('textarea');
    // Set value (string to be copied)
    el.value = str;
    // Set non-editable to avoid focus and move outside of view
    el.setAttribute('readonly', '');
    el.style = {position: 'absolute', left: '-9999px'};
    document.body.appendChild(el);
    // Select text inside element
    el.select();
    // Copy text to clipboard
    document.execCommand('copy');
    // Remove temporary element
    document.body.removeChild(el);
}

/**
 * Gallery type 1: Mace Gallery
 */
function addCopyButtonMaceGallery()
{
    const headerTitle = document.querySelector('.g1-gallery-title');
    const frames = Array.from(document.querySelectorAll('.g1-gallery-frame'));
    const urls = frames.map((x) => {
        return x.dataset.g1ShareImage;
    });

    const button = document.createElement('button');
    button.appendChild(document.createTextNode('Copy direct image URLs (list)'));

    button.addEventListener('click', function() {
        copyStringToClipboard(urls);
    }, false);

    headerTitle.insertAdjacentElement('afterend', button);
}

/**
 * Gallery type 2: Gallery Items (class name: `.gallery-item`)
 */
function addCopyButtonGalleryItems(galleryItems)
{
    const entryTitle = document.querySelector('.entry-title');
    const items = Array.from(galleryItems);
    const imageUrls = [];

    for (const item of items)
    {
        const image = item.querySelector('img');
        const url = image.src;

        imageUrls.push(url);
    }

    const button = document.createElement('button');
    button.appendChild(document.createTextNode('Copy direct image URLs (list)'));

    button.addEventListener('click', function() {
        copyStringToClipboard(imageUrls);
    }, false);

    entryTitle.insertAdjacentElement('beforeend', button);
}

/**
 * Type 1
 */
const teaser = document.querySelector('.mace-gallery-teaser');
if (teaser) {
    teaser.addEventListener('click', function() {
        // Short delay so the gallery can properly load.
        setTimeout(function() {
            addCopyButtonMaceGallery();
        }, 1500);
    });
}

/**
 * Type 2
 */
const galleryItems = document.querySelectorAll('.gallery-item');
if (galleryItems.length > 0)
{
    addCopyButtonGalleryItems(galleryItems);
}