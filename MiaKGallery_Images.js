// ==UserScript==
// @name        Mia K Gallery - Get Image URLs
// @namespace   Get list of image URLs
// @match       https://www.miak.gallery/*
// @grant       none
// @version     1.0.0
// @author      Marcus
// @description 3/1/2020, 2:34:28 PM
// ==/UserScript==

function copyToClipboard(str)
{
    const el = document.createElement('textarea');
    el.value = str;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}

let images = [];
async function getImageUrls()
{
    const elements = Array.from(document.querySelectorAll('#slideshow img'));
    images = elements.map((img) => {
        const url = new URL(img.src);
        return url.origin + url.pathname;
    });

    console.log(elements);
    console.log(images);
}

async function addCopyBtn()
{
    const button = document.createElement('button');
    button.appendChild(document.createTextNode(`Copy ${images.length} image URLs to clipboard`));

    button.addEventListener('click', function() {
        copyToClipboard(images.join('\n'));
        //alert(`Copied ${images.length} image URLs to clipboard.`);
    }, false);

    const content = document.querySelector('#mainContent');
    content.insertAdjacentElement('afterbegin', button);
}

window.addEventListener('load', async () => {
    if (!document.querySelector('#slideshow')) {
        console.log('[MiaK - Get Image URLs] No slideshow element found, returning.');
        return;
    }

    setTimeout(async () => {
        await getImageUrls();
        await addCopyBtn();
    }, 1500);
});