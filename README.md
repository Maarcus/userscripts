# Userscripts

## License

GNU General Public License, version 3: [LICENSE](./LICENSE)

## wat is dis

Userscripts I've made to make the browser experience on various websites better, though some userscripts are made with more niche things in mind (to work with other software).

Most of my userscripts are tested on [Violentmonkey](https://violentmonkey.github.io/), with [Mozilla Firefox](https://www.mozilla.org/en-US/firefox/). I can't guarantee that my scripts work with other userscript managers or other browsers, but in general I try to make them compatible as much as possible.

Unless I've been lazy, each userscript should at the very least have a short description at the top of the script that explains what they do.