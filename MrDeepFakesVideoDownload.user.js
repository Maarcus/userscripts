// ==UserScript==
// @name        MrDeepFakes - Video Downloader
// @namespace   github.com/M-rcus
// @match       https://mrdeepfakes.com/video/*/*
// @grant       none
// @version     1.1.0
// @author      Maarcus
// @description Downloads videos from MrDeepFakes (for free).
// @grant       GM_download
// @grant       unsafeWindow
// @downloadUrl https://gist.github.com/M-rcus/9bbdc79fc72bc30e9f554c0923960243/raw/MrDeepFakesVideoDownload.user.js
// @updateUrl   https://gist.github.com/M-rcus/9bbdc79fc72bc30e9f554c0923960243/raw/MrDeepFakesVideoDownload.user.js
// ==/UserScript==

/**
 * Known issues:
 * - Downloads can be slow.
 *   - Workaround: None.
 *     I think this is just MrDeepFakes throttling download speeds (understandable, the videos are meant to be streamed). Not much I can do.
 *
 * - Downloads may fail the first attempt and download an invalid file (usually instantly).
 *   - Workaround: Try again. Usually it works the second try. I believe it might be a race condition, but idfk.
 *     From what I can tell this mostly happens when you instantly click 'Download'.
 *     Waiting a few seconds for the page to completely load might also work, but it's a bit hard to troubleshoot consistently.
 *
 * - Qualities past 720p require logging into MrDeepFakes
 *   - Workaround: For now the script silently handles this in the background and tries the next best quality (typically 720p).
       If you want the best quality available, you need to be logged in.
 */

/**
 * Due to the API (`GM_download`) used, this is *not* compatible with Greasemonkey, as it's not implemented in GM.
 * Should work fine with both Violentmonkey or Tampermonkey though.
 */

const saveAs = (url, filename) => {
    const dlBtn = document.querySelector('#free-dl-btn');
    console.log(`SaveAs - URL: ${url}`);
    console.log(`SaveAs - Filename: ${filename}`);

    const downloadParams = {
        method: 'GET',
        url: url,
        name: filename,
        saveAs: true,
        headers: {
            Referer: window.location.href,
            'User-Agent': navigator.userAgent,
        },
        onload: (response) => {
            dlBtn.innerHTML = `Download complete!`;
            dlBtn.removeAttribute('disabled');
        },
        onprogress: (results) => {
            const {total, loaded} = results;
            const percentFloat = loaded / total;

            /**
             * Percent with max 2 decimals.
             */
            const percent  = (percentFloat * 100).toFixed(2);
            const progress = `Download progress: ${percent}%`;
            dlBtn.innerHTML = progress;
        },
        onerror: (obj) => {
            console.error(obj);
            dlBtn.innerHTML = 'Error occurred (see console)';
            dlBtn.removeAttribute('disabled');
        },
    };

    GM_download(downloadParams);
};

/**
 * Generate the HTML for the button and add it to the page
 * 
 * @param  {string} url
 * @param  {string} videoTitle
 * @param  {string} videoQuality
 * @return {void}
 */
const overwriteDownloadButton = (url, videoTitle, videoQuality) => {
    /**
     * Remove old download button (if it exists).
     */
    const oldDlBtn = document.querySelector('#free-dl-btn');
    if (oldDlBtn)
    {
        oldDlBtn.remove();
    }

    const button = document.createElement('button');
    button.setAttribute('class', 'toggle-button');
    button.setAttribute('id', 'free-dl-btn');
    button.appendChild(document.createTextNode(`Download video [${videoQuality}]`));

    /**
     * Mainly for debugging purposes.
     */
    const videoFilename = `${videoTitle} [${videoQuality}].mp4`;
    button.setAttribute('data-title', videoFilename);
    button.setAttribute('data-url', url);

    button.addEventListener(
        'click',
        function () {
            console.log('Downloading video...');
            button.setAttribute('disabled', '1');

            saveAs(url, videoFilename);
        },
        false,
    );

    const header = document.querySelector('.headline h1');
    header.insertAdjacentElement('afterend', button);
};

/**
 * Generate button with URL and quality (for filename)
 * 
 * @param  {string} videoUrl
 * @param  {string} videoQuality
 * @return {void}
 */
const generateBtn = (videoUrl, videoQuality) => {
    /**
     * Get the video title and replace any "weird" characters.
     */
    const videoTitleHeader = document.querySelector('.headline h1');
    const videoTitle = videoTitleHeader.textContent.replace(/[^0-9a-z_\- ]/gi, '_');

    overwriteDownloadButton(videoUrl, videoTitle, videoQuality);
};

/**
 * Attempts to find proper URLs so that we can download the thingies...
 * 
 * @param  {object} flashvars
 * @return {void}
 */
const findUrl = (flashvars, video) => {
    /**
     * Temporary variable for storing what we get.
     */
    let urlSelection = 'video_url';

    /**
     * Set a fallback URL...
     */
    let videoUrl = video.src;
    const fvKeys = Object.keys(flashvars);

    /**
     * Available quality varies per video.
     *
     * From what I can tell, the qualities can be 'mapped' like this:
     * - alt_url3: 1080p
     * - alt_url2: 720p
     * - alt_url: 480p
     * - video_url: 360p (default)
     * 
     * @type {Array}
     */
    const possibleUrls = [
        'video_alt_url3',
        'video_alt_url2',
        'video_alt_url',
    ];

    /**
     * Try to get the highest quality available.
     * At some point, maybe this script should have a quality selection.
     */
    for (const urlKey of possibleUrls)
    {
        if (! fvKeys.includes(urlKey))
        {
            continue;
        }

        console.log(`Video URL key found: ${urlKey}`);
        urlSelection = urlKey;

        /**
         * Our previous method already had `rnd` appended to the URL
         * But we don't have that luxury right now.right
         *
         * Luckily they seem to use an arbitrary value and just generating a UNIX timestamp
         * so we'll do the same :)
         *
         * Sometimes the `video_alt_url` values will include a `function/0/` for reasons I'm not sure of.
         * (I don't think the URLs are being evaluated, so idk). Easy enough to just replace the shit.
         */
        videoUrl = flashvars[urlSelection]
                   .replace(/^.*https\:/, 'https:')
                   .trim();

        /**
         * Turns out that anything past 720p requires a login.
         * We'll just silently error (in console) that if people want 1080p, they need to log in.
         */
        if (videoUrl.includes('https://mrdeepfakes.com/?login'))
        {
            const qualitySelect = `${urlSelection}_text`;
            const quality = flashvars[qualitySelect] || 'NA';
            console.error(`Login required to download video in quality: ${quality}. Finding next available quality.`);

            // Continue the loop to select a new valid quality.
            continue;
        }

        break; // Break the loop, or else later values will overwrite it.
    }

    /**
     * The quality is based on the `key` we found.
     * Luckily the format is pretty simple as it's just `keyName_text` to get the video quality.
     *
     * The quality is only really used in the filename and is purely for organization purposes.
     */
    const qualitySelection = `${urlSelection}_text`;
    const videoQuality = flashvars[qualitySelection];

    console.log(`Video URL: '${videoUrl}'`);
    console.log(`Video Quality: ${videoQuality}`);
    generateBtn(videoUrl, videoQuality);
};

window.addEventListener('DOMContentLoaded', () => {
    let intervalCount = 0;
    const delay = 300;
    const waitForPageLoad = setInterval(() => {
        intervalCount++;

        /**
         * Clear interval after 15 seconds
         */
        if (intervalCount * delay > 15000) {
            console.error('Unable to find flashvars/video in time, stopping all attempts.');
            clearInterval(waitForPageLoad);
            return;
        }

        /**
         * No flashvars or video found, wait for next interval tick.
         *
         * The reason we wait for the video to be ready as well is to make it less likely for errors to occur.
         * It seems to work to also just wait an arbitrary amount of time (tested with 1.5s), but just doing a `setTimeout()` is a bit scuffed.
         * To be honest, this method is already pretty scuffed as well, but I'm running out of ideas at this point... and it seems to be a bit more reasonable than just waiting X amount of time.
         */
        const flashvars = unsafeWindow.flashvars;
        const video = document.querySelector('video');
        if (!flashvars || !video) {
            return;
        }

        /**
         * #performance
         */
        clearInterval(waitForPageLoad);
        findUrl(flashvars, video);
    }, delay);
});